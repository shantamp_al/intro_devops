# Introduction to DevOps

The DevOps movement start sometime in 2007/2008, when IT operations and software development teams raised the issues they faced while working in the industry. 

Before DevOps came about, software development had to guess the estimated time it would take to build their infrastructure. The problem with this is that the team would work on the project but the client would not be able to see until it was finished. This caused issues as if there was something wrong, the client could only flag once sofware was built, which resulted in the product being returned to the development team to fix. This affected many people such as...

* Development Team
* Clients
* Project Managers / Delivery Managers.

As for operations (Ops) their job was hard aswell. These are the kind of these they had to think about before setting up a website:

* Using data centres 
* 1000 of computers, lots of wires and lots of dust.
* Staff to clean the equipment
* Security check the staff and pay them well not to leave
* Backup for power
* Backup data centre in case internet crashes
* Fire protocols

All of these things would cost a lot of time and a lot of money.



